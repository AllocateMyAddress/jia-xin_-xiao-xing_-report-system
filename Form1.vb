﻿Option Explicit On

Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Text
Imports System.Threading
Imports Microsoft.Reporting.WinForms
Imports Microsoft.VisualBasic.Devices

'跳过老代码， 为了备用。。1
#Const OldCodeEnable = False

Public Class Form1



    Public ISLoadingReport = False
    Dim K1ConnOBJ As SqlConnection = Nothing
    Dim K2ConnOBJ As SqlConnection = Nothing
    Public EnvConfig As DefEnvConfig = DefEnvConfig.k2

    '全局 启用日期区间
    Public cbxEnableDateRange_Visible = False
    Public SkipK1MIDEnable As Boolean = True
    Public SkipK2MIDEnable As Boolean = False

    Dim ImageList_Loading = GifAmin.GetGifFrames()
    Dim UIINIT_Finish = False


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        swKeyDownCoolDown.Start()
        '读取配置文件!
        Dim usingKN = ConfigurationManager.AppSettings("UsingK1|K2|DevK1|DevK2")
        EnvConfig = Microsoft.VisualBasic.Switch(usingKN = "K1", DefEnvConfig.k1, usingKN = "K2", DefEnvConfig.k2, usingKN = "DevK1", DefEnvConfig.devK1, usingKN = "DevK2", DefEnvConfig.devK2)

        dtPicker1.Value = Date.Now.AddDays(-1)
        dtPicker2.Value = dtPicker1.Value.AddDays(1)
        TmpDtPk1 = dtPicker1.Value
        TmpDtPk2 = dtPicker2.Value

        UIText_SqlServerConnStatus.ForeColor = Color.Black
        ReportViewer1.AutoScroll = True
        ReportViewer1.VerticalScroll.Visible = True

        '控制测试模式!
        cbxTestMode.Checked = False

        If cbxTestMode.Checked Then
            cbxTestMode.Visible = True
            'dtPicker1.Value = Date.Parse("2020-2-1")
        End If


        Select Case EnvConfig
            Case DefEnvConfig.k1
                Me.Text = "一科报表查看器"
            Case DefEnvConfig.k2
                Me.Text = "二科报表查看器"
            Case Else
        End Select


        HideFliters(50)
        Me.WindowState = FormWindowState.Normal

        btnQuery.ForeColor = Color.White

        AsyncToDo(Sub()
                      Thread.Sleep(300)
                      Me.Invoke(Sub()
                                    Me.WindowState = FormWindowState.Maximized



                                    'DelaySync(150)
                                    UI_Init()
                                    'ReLoadFlowPenelFliter()
                                    ShowFliters(50)
                                    UIINIT_Finish = True
                                    '清掉Form设计器上的 "设备生产量", "设备开动率", "锭位故障率", "整机故障时长统计"
                                    cbbxRptType.Items.Clear()
                                    '为了加载测试用的代码。
                                    If cbxTestMode.Checked Then
                                        LoadReport.InitRptTypeByMainForm1()
                                    End If

                                    cbbxRptType.Items.AddRange({"设备生产量", "设备开动率", "锭位故障率", "整机故障时长统计"})
                                    cbbxRptType.SelectedIndex = 0
                                End Sub)

                      Thread.Sleep(300)
                      RunAnimationServerForLoadingAnim()

                  End Sub)

        Me.Text += ConfigurationManager.AppSettings("Version")

        '这是载入对应的报表， 为了更好地管理。
        LoadReport.FrmPtr = Me

        LoadReport_DevK1DS.FrmPtr = Me
        LoadReport_DevK2DS.FrmPtr = Me


        Dim K1ConnString = ""
        Dim K2ConnString = ""
#If DEBUG = True Then
        'ConnString = "Data Source=Win7-2019DFSPXL\MSSQLEXPRESS2014;Database=SYC_NSJ1;uid=sa;pwd=123"
        K1ConnString = ConfigurationManager.ConnectionStrings("User1keConnStr").ConnectionString
        K2ConnString = ConfigurationManager.ConnectionStrings("User2keConnStr").ConnectionString



#ElseIf RELEASE = True Then
        '这个字符串是 OLEDB 需要 Provider的
        '实际上情况再连接对应的SQL服务器 ， 实际情况请编译成 Release 或再定义一个 1科 2科
        'K1ConnString = "Data Source=Win7-2019DFSPXL\MSSQLEXPRESS2014;Database=SYC_NSJ1;uid=sa;pwd=123"
        K1ConnString = ConfigurationManager.ConnectionStrings("User1keConnStr").ConnectionString
        K2ConnString = ConfigurationManager.ConnectionStrings("User2keConnStr").ConnectionString




#End If
        Try
            Select Case EnvConfig
                Case DefEnvConfig.k1
                    K1ConnOBJ = New SqlClient.SqlConnection(K1ConnString)

                    Try
                        K1ConnOBJ.Open()
                    Catch ex As Exception
                        MessageBox.Show("一科服务器连接失败")
                        隐藏UI连接字符串()
                        Throw ex
                    End Try
                    UIText_SqlServerConnStatus.Text = K1ConnOBJ.DataSource + " 连接成功"



                Case DefEnvConfig.k2
                    K2ConnOBJ = New SqlClient.SqlConnection(K2ConnString)
                    Try
                        K2ConnOBJ.Open()
                    Catch ex As Exception
                        MessageBox.Show("二科服务器连接失败")
                        隐藏UI连接字符串()
                        Throw ex
                    End Try
                    UIText_SqlServerConnStatus.Text = K2ConnOBJ.DataSource + " 连接成功"
                Case Else

            End Select



            cbbxBrand.Items.Clear()
            cbbxBrand.Items.Add("*")
            Dim ds = New DataSet
            Dim tmpret = ExecuteSQLCMD(Interaction.Switch(EnvConfig = DefEnvConfig.k1,
                                                          K1ConnOBJ, EnvConfig = DefEnvConfig.k2, K2ConnOBJ),
                                       ds, "Select distinct Brand from  MachineClass")
            If tmpret.isSuccess Then
                For Each xx As DataRow In ds.Tables(0).Rows
                    cbbxBrand.Items.Add(xx.ItemArray(0).ToString().Trim().Replace(" ", ""))
                Next

            End If






            'Dim ds As New DataSet
            'Dim strSQL = "select * , LYieldNum+RYieldNum AS YieldNum from REC_CLASS"
            'Dim i As Integer
            ''strSQL = "  Select Case* From [AlarmDuration]Where ALLMA_J_1 = 2"
            'Dim Tmp = ExecuteSQLCMD(ConnOBJ, ds, strSQL)


            'If Tmp.isSuccess Then
            '    ShowDayClassReport(Nothing, True)
            'End If

            'https://docs.microsoft.com/zh-cn/previous-versions/sql/sql-server-2008-r2/ms345237(v=sql.105)
        Catch ex As Exception
            UIText_SqlServerConnStatus.Text = "服务器连接失败"
            UIText_SqlServerConnStatus.ForeColor = Color.IndianRed
            MessageBox.Show(ex.Message)

        End Try


        隐藏UI连接字符串()


    End Sub

    Public Sub RunAnimationServerForLoadingAnim()

        Dim sw = New Stopwatch()
        sw.Start()
        Dim DoOncePerLoadingReport = True

        While True
            If ISLoadingReport Then
                Me.Invoke(Sub()
                              Pbx1.Location = New Point(ReportViewer1.Location.X + ReportViewer1.Width / 2 - Pbx1.Width / 2, ReportViewer1.Location.Y + ReportViewer1.Height / 2 - Pbx1.Height / 2)
                              Pbx1.Visible = True
                              For index = 1 To ImageList_Loading.Count - 1
                                  Pbx1.Image = ImageList_Loading(index)
                                  Thread.Sleep(30)
                                  Application.DoEvents()
                              Next
                          End Sub)
            Else
                If DoOncePerLoadingReport Then
                    sw.Restart()
                    DoOncePerLoadingReport = False
                End If
                If sw.ElapsedMilliseconds > 1 Then
                    sw.Restart()
                    If Not IsDisposed Then
                        Me?.Invoke(Sub()
                                       If Not (Pbx1 Is Nothing) Then
                                           Pbx1.Visible = False
                                       End If
                                       ISLoadingReport = False
                                   End Sub)
                    End If
                    DoOncePerLoadingReport = True

                End If

            End If
            If FrmClosing Then
                Exit While
            End If

            Application.DoEvents()
            Thread.Sleep(10)
        End While

    End Sub



    Private Sub 隐藏UI连接字符串()
        Task.Run(Sub()
                     Thread.Sleep(1500)
                     Me.Invoke(Sub()
                                   UIText_SqlServerConnStatus.Text = ""

                                   UIText_SqlServerConnStatus.Visible = False
                                   UI_TextSQLSvr.Visible = False
                               End Sub)

                 End Sub)
    End Sub

    Dim InitFinish = False

    Sub UI_Init()
        'lblCBBXRptType.Location = New Point(lblCBBXRptType.Location.X - 45, lblCBBXRptType.Location.Y)
        'cbbxRptType.Location = New Point(cbbxRptType.Location.X - 45, cbbxRptType.Location.Y)
        cbbxRptType.SelectedIndex = 0
        cbbxCycle.SelectedIndex = 0
        cbbxBrand.SelectedIndex = 0
        dtPicker2.Enabled = True
    End Sub

    Private Shared Function GetColumnNames(ds As DataSet) As List(Of String)
        Dim ColList As List(Of String) = New List(Of String)(100)
        Dim MainTable = ds.Tables(0)
        For Each x As DataColumn In MainTable.Columns
            ColList.Add(x.ColumnName)
        Next

        Return ColList
    End Function


    Private Sub SwitchReport()
        Select Case EnvConfig
            Case DefEnvConfig.k1
                Me.Text = "一科报表查看器"
            Case DefEnvConfig.k2
                Me.Text = "二科报表查看器"
            Case Else
        End Select



        '跟环境自动切换
        Select Case EnvConfig
            Case DefEnvConfig.k1
                LoadReport.GetReport(EnvConfig)
            Case DefEnvConfig.k2
                LoadReport.GetReport(EnvConfig)
            Case DefEnvConfig.devK1
                LoadReport_DevK1DS.ShowPeport(EnvConfig)
            Case DefEnvConfig.devK2
                LoadReport_DevK2DS.ShowPeport(EnvConfig)


        End Select
    End Sub



    Private Sub ShowTestReport(dtFilter As Date, _ReportEmbeddedResource As String, TestMode As Boolean)
        If TestMode Then
            dtFilter = dtPicker1.Value
        End If

        Dim dta = New SYC_NSJ1DataSetTableAdapters.MachineBrand_GetOneDayClassYieldByDateTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim params() As ReportParameter
        rptALL.Name = "DataSet1"

        rptALL.Value = dta.GetData(dtFilter, 1)
        params = {
                 New ReportParameter("RptDate", dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("DateUnit", RptDateUnit.Day.ToString())
                 }
        'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),


        ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rptALL)

        'ReportViewer1.LocalReport.SetParameters(params)
        ReportViewer1.LocalReport.Refresh()
        Me.ReportViewer1.RefreshReport()



    End Sub

    Private Sub cbbxRptType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbxRptType.SelectedIndexChanged
        If UIINIT_Finish Then
            LoadReport.InitRptCycleByMainForm1()
        End If
    End Sub

    Public Sub InitPhase_LoadCbbxRptType(MainFrmPtr As Form1, ExtAction As Action(Of Form1))
        '执行扩展方法，因为 需要 增加 测试的报表。
        If cbxTestMode.Checked Then
            ExtAction(MainFrmPtr)
        End If

        '再执行它默认(非测试模式的)  报表周期。
        Select Case cbbxRptType.SelectedItem?.ToString()
            'Case "机型班报"
            '    cbbxCycle.Items.Clear()
            '    cbbxCycle.Items.AddRange({"班报", "日报", "月报", "年报"})
            'Case "机型日报"
            '    cbbxCycle.Items.Clear()
            '    cbbxCycle.Items.AddRange({"班报", "日报", "月报", "年报"})
            Case "设备生产量"
                cbbxCycle.Items.Clear()
                cbbxCycle.Items.AddRange({"日报", "月报", "年报"})
                Application.DoEvents()
                cbbxCycle.SelectedIndex = 0

                UITextBrandShow(True)
            Case "设备开动率"
                cbbxCycle.Items.Clear()
                cbbxCycle.Items.AddRange({"日报", "月报", "年报"})
                Application.DoEvents()
                cbbxCycle.SelectedIndex = 0

                UITextBrandShow(True)
            Case "锭位故障率"
                cbbxCycle.Items.Clear()
                cbbxCycle.Items.AddRange({"日报", "月报", "年报"})
                Application.DoEvents()
                cbbxCycle.SelectedIndex = 0

                UITextBrandShow(True)
            Case "整机故障时长统计"
                cbbxCycle.Items.Clear()
                cbbxCycle.Items.AddRange({"月报"})
                Application.DoEvents()
                cbbxCycle.SelectedIndex = 0
                UITextBrandShow(False)
        End Select


    End Sub

    Public Sub UITextBrandShow(isVisible As Boolean)
        UITEXT_Brand.Visible = isVisible
        cbbxBrand.Visible = isVisible
    End Sub

    Private Sub cbxEnableDateRange_CheckedChanged(sender As Object, e As EventArgs) Handles cbxEnableDateRange.CheckedChanged, cbxTestMode.CheckedChanged
        'If cbxEnableDateRange.Checked Then
        '    dtpicker2.Visible = True
        '    lblDtp2.Visible = True
        '    cbbxRptType.Location = New Point(cbbxRptType.Location.X + 40, cbbxRptType.Location.Y)
        '    lblCBBXRptType.Location = New Point(lblCBBXRptType.Location.X + 40, lblCBBXRptType.Location.Y)
        '    cbxEnableDateRange.Location = New Point(cbxEnableDateRange.Location.X + 343, cbxEnableDateRange.Location.Y)
        'Else
        '    cbbxRptType.Location = New Point(cbbxRptType.Location.X - 40, cbbxRptType.Location.Y)
        '    lblCBBXRptType.Location = New Point(lblCBBXRptType.Location.X - 40, lblCBBXRptType.Location.Y)
        '    cbxEnableDateRange.Location = New Point(cbxEnableDateRange.Location.X - 343, cbxEnableDateRange.Location.Y)
        '    dtpicker2.Visible = False
        '    lblDtp2.Visible = False
        'End If


        If cbxEnableDateRange.Checked Then
            dtPicker2.Visible = True
            lblDtp2.Visible = True
        Else
            dtPicker2.Visible = False
            lblDtp2.Visible = False
        End If

    End Sub


    Private Sub btnQuery_Click(sender As Object, e As EventArgs) Handles btnQuery.Click


        Me.btnQuery.Enabled = False
        Try
            SwitchReport()
        Catch ex As Exception
            Me.btnQuery.Enabled = True
            Me.btnQuery.ForeColor = Color.Black
            Me.btnQuery.BackColor = Color.LimeGreen
            Throw ex
        End Try
        'DelaySync(1500)

        Me.btnQuery.Enabled = True
    End Sub

    Private Sub cbbxBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbxBrand.SelectedIndexChanged
        If UIINIT_Finish Then
            btnQuery_Click(Nothing, New EventArgs())
        End If
    End Sub

    Dim FrmClosing = False

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing

        K1ConnOBJ?.Close()
        K2ConnOBJ?.Close()
        FrmClosing = True
    End Sub


    Private Sub Panel1_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles Panel1.PreviewKeyDown

    End Sub

    Dim scrollX = 1, scrollY = 1

    Private Sub BtnCanel_Click(sender As Object, e As EventArgs) Handles BtnCanel.Click
        ReportViewer1.CancelRendering(3000)
        ReportViewer1.AutoScrollPosition = New Point(scrollX, scrollY)
        scrollX += 1
    End Sub




    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles dtPicker1.ValueChanged

        '这条件判断 为了 过滤掉 初始化的时 的控制值设置。
        If UIINIT_Finish Then
            TmpDtPk1 = dtPicker1.Value
            'If dtPicker1.Value >= dtPicker2.Value Then
            '    MsgBox("起始时间 不能 大于等于 结束时间")
            '    dtPicker1.Value = TmpDtPk1
            'End If
            If dtPicker1.Value >= Now Then
                MsgBox("起始时间 不能 大于 当前时间")
                dtPicker1.Value = TmpDtPk1
            End If
        End If


    End Sub

    Private Sub DateTimePicker2_ValueChanged(sender As Object, e As EventArgs) Handles dtPicker2.ValueChanged
        'If UIINIT_Finish Then

        '    If dtPicker2.Value >= dtPicker1.Value Then
        '        MsgBox("结束时间 不能 小于或等于 起始时间")
        '        dtPicker2.Value = TmpDtPk2
        '    End If
        '    If dtPicker2.Value >= Now Then
        '        MsgBox("起始时间 不能 大于 当前时间")
        '        dtPicker2.Value = TmpDtPk2
        '    End If
        'End If


    End Sub

    '为了 可以回滚 日期修改之前的日期
    Dim TmpDtPk1 = New DateTime()
    Dim TmpDtPk2 = New DateTime()

    Private Sub dtPicker1_DropDown(sender As Object, e As EventArgs) Handles dtPicker1.DropDown
        TmpDtPk1 = dtPicker1.Value
    End Sub

    Private Sub dtPicker2_DropDown(sender As Object, e As EventArgs) Handles dtPicker2.DropDown
        TmpDtPk2 = dtPicker2.Value
    End Sub


    Sub AsyncToDo(AsyncAction As Action)
        Task.Run(Sub()
                     AsyncAction()
                 End Sub
             )
    End Sub


    Private Sub HideFliters(ms As Int16)
        Me.Label1.Visible = False
        ComonFuncs.DelaySync(ms)
        Me.dtPicker1.Visible = False
        ComonFuncs.DelaySync(ms)
        lblDtp2.Visible = False
        ComonFuncs.DelaySync(ms)
        dtPicker2.Visible = False
        ComonFuncs.DelaySync(ms)
        Me.Label2.Visible = False
        ComonFuncs.DelaySync(ms)
        Me.cbbxCycle.Visible = False
        ComonFuncs.DelaySync(ms)
        Me.UITEXT_Brand.Visible = False
        ComonFuncs.DelaySync(ms)
        Me.cbbxBrand.Visible = False
        ComonFuncs.DelaySync(ms)
        Me.cbxEnableDateRange.Visible = False
    End Sub


    Private Sub ShowFliters(ms As Int16)
        Me.Label1.Visible = True
        ComonFuncs.DelaySync(ms)
        Me.dtPicker1.Visible = True
        ComonFuncs.DelaySync(ms)

        If cbxEnableDateRange.Checked Then
            lblDtp2.Visible = True
            ComonFuncs.DelaySync(ms)
            dtPicker2.Visible = True
        Else
            lblDtp2.Visible = False
            ComonFuncs.DelaySync(ms)
            dtPicker2.Visible = False
        End If

        ComonFuncs.DelaySync(ms)
        Me.Label2.Visible = True
        ComonFuncs.DelaySync(ms)
        Me.cbbxCycle.Visible = True
        ComonFuncs.DelaySync(ms)
        Me.UITEXT_Brand.Visible = True
        ComonFuncs.DelaySync(ms)
        Me.cbbxBrand.Visible = True
        ComonFuncs.DelaySync(ms)
        Me.cbxEnableDateRange.Visible = cbxEnableDateRange_Visible
    End Sub

    Private Sub ShowFliterQuiet()
        Me.Label1.Visible = True
        Me.dtPicker1.Visible = True
        lblDtp2.Visible = True
        dtPicker2.Visible = True
        Me.Label2.Visible = True
        Me.cbbxCycle.Visible = True
        Me.UITEXT_Brand.Visible = True
        Me.cbbxBrand.Visible = True
        Me.cbxEnableDateRange.Visible = False
    End Sub

    Private Sub Panel1_Scroll(sender As Object, e As ScrollEventArgs) Handles Panel1.Scroll
        e.NewValue += 1
        MessageBox.Show("")
    End Sub

    Private Sub ReportViewer1_Scroll(sender As Object, e As ScrollEventArgs) Handles ReportViewer1.Scroll
        MessageBox.Show(e.ScrollOrientation.ToString())

    End Sub

    Private Sub BtnTestAsyncQuery_Click(sender As Object, e As EventArgs) Handles BtnTestAsyncQuery.Click
        Dim K1CStr = ConfigurationManager.ConnectionStrings("User1keConnStr").ConnectionString
        Dim sqlobj = New SqlClient.SqlConnection(K1CStr)
        sqlobj.Open()
        Dim sqlobj1 = New SqlClient.SqlConnection(K1CStr)
        sqlobj1.Open()
        Dim sqlobj2 = New SqlClient.SqlConnection(K1CStr)
        sqlobj2.Open()
        Dim sqlobj3 = New SqlClient.SqlConnection(K1CStr)
        sqlobj3.Open()

        Dim ds = New DataSet()
        Dim cmd = New SqlCommand("_v3_Rpt_GetDayYieldFromOP_Class_FixVersion", sqlobj)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(New SqlParameter() {
                                New SqlParameter("@StartDate", "2021-7-2"),
                                New SqlParameter("@TestMode", 1)
                                })


        Dim cmd1 = New SqlCommand("_v3_Rpt_GetDayYieldFromOP_Class_FixVersion", sqlobj1)
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.Parameters.AddRange(New SqlParameter() {
                                New SqlParameter("@StartDate", "2021-7-2"),
                                New SqlParameter("@TestMode", 1)
                                })

        Dim cmd2 = New SqlCommand("_v3_Rpt_GetDayYieldFromOP_Class_FixVersion", sqlobj2)
        With cmd2
            .CommandType = CommandType.StoredProcedure
            .Parameters.AddRange(New SqlParameter() {
                                    New SqlParameter("@StartDate", "2021-7-2"),
                                    New SqlParameter("@TestMode", 1)
                                    })
        End With



        Dim sw1 = New Stopwatch()
        sw1.Restart()
        Dim ret = cmd.ExecuteReaderAsync()
        Dim ret1 = cmd1.ExecuteReaderAsync()
        Dim ret2 = cmd2.ExecuteReaderAsync()

        MsgBox("costTime=" & sw1.ElapsedMilliseconds.ToString() & "毫秒")
        sw1.Restart()


        ret.Wait()
        ret1.Wait()
        ret2.Wait()

        ret1.Result.Close()
        cmd1.Connection.Close()
        Dim xx = sqlobj1.Database


        Dim listrow = ret.Result.GetSchemaTable().AsEnumerable()
        sqlobj.Close()
        sqlobj1.Close()
        sqlobj2.Close()
        sqlobj3.Close()





        MsgBox("costTime=" & sw1.ElapsedMilliseconds.ToString() & "毫秒")


        'Dim Adapter = New SqlDataAdapter(cmd)

        'Adapter.Fill(ds, "testTable")
        'Dim xx = ds.Tables(0).AsEnumerable().ToList()

        'Adapter.Fill(ds, "testTable")
        'xx.AddRange(ds.Tables(0).AsEnumerable())

        'Adapter.Fill(ds, "testTable")
        'xx.AddRange(ds.Tables(0).AsEnumerable())

    End Sub
    Dim swKeyDownCoolDown As Stopwatch = New Stopwatch()

    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.F5 Then

            If swKeyDownCoolDown.ElapsedMilliseconds > 1000 Then
                btnQuery_Click(btnQuery, New EventArgs())
                swKeyDownCoolDown.Restart()
            End If
        End If
    End Sub

    Private Sub Form1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            btnQuery_Click(btnQuery, New EventArgs())
        End If
    End Sub

    Private Sub cbbxCycle_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbxCycle.SelectedIndexChanged
        Select Case cbbxCycle.SelectedText
            Case "日报"
                dtPicker1.CustomFormat = "yyyy年MM月dd日"
            Case "月报"
                dtPicker1.CustomFormat = "yyyy年MM月"
            Case "年报"
                dtPicker1.CustomFormat = "yyyy年"
            Case Else

        End Select

    End Sub
End Class
