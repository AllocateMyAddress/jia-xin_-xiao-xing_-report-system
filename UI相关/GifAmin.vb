﻿Imports System.IO

Class GifAmin

    Shared Function GetGifFrames() As List(Of Image)

        'Dim imgGif = Image.FromFile("Gif_Loading.gif", True)
        Dim imgGif = Global.ReportSystem.My.Resources.Gif_Loading
        Dim ImgFrmDim = New Imaging.FrameDimension(imgGif.FrameDimensionsList(0))

        'Determine the number of frames in the image
        'Note that all images contain at least 1 frame,
        'but an animated GIF will contain more than 1 frame.

        Dim n = imgGif.GetFrameCount(ImgFrmDim)

        Dim ms(n) As MemoryStream
        Dim imgList = New List(Of Image)(30)
        For i = 0 To n - 1
            imgGif.SelectActiveFrame(ImgFrmDim, i)
            'imgGif.Save(String.Format("Frame{0}.png", i), Imaging.ImageFormat.Png)
            ms(i) = New MemoryStream()
            imgGif.Save(ms(i), Imaging.ImageFormat.Png)
            imgList.Add(Image.FromStream(ms(i)))

        Next
        Return imgList
    End Function
End Class
