﻿Public Module ConstDefineModule

    Public Enum RptDateUnit
        DateRange
        Day
        Month
        Year
        TenYear
    End Enum

    Public Enum YieldUnit
        '米'
        Metre
        '克'
        Gram
        '吨'
        Ton
    End Enum


    Public Enum DefEnvConfig
        devK1
        devK2
        k1
        k2
    End Enum
End Module

