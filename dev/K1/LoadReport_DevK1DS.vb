﻿Imports Microsoft.Reporting.WinForms

Public Class LoadReport_DevK1DS
    Public Shared FrmPtr As Form1

    Shared Sub ShowPeport(EvnPrefix As DefEnvConfig)
        If FrmPtr.cbxTestMode.Checked Then
            FrmPtr.dtPicker1.Value = Date.Parse("2021-4-10")
        End If


        Select Case FrmPtr.cbbxRptType.SelectedItem?.ToString()
            Case "机型班报"
                LoadReport_DevK1DS.ShowClassReport(FrmPtr.dtPicker1.Value, True, "ReportSystem.MB" + EvnPrefix.ToString() + "_ReportByClass.rdlc", False)
            Case "机型日报"

            Case "设备生产量"
                Select Case FrmPtr.cbbxCycle.SelectedItem
                    Case "日报"
                        LoadReport_DevK1DS.ShowDayMonthYear_PatternReport(FrmPtr.dtPicker1.Value, RptDateUnit.Day, "ReportSystem.MB" + EvnPrefix.ToString() + "_ReportDayMonthYear.rdlc", False)
                    Case "月报"
                        LoadReport_DevK1DS.ShowDayMonthYear_PatternReport(FrmPtr.dtPicker1.Value, RptDateUnit.Month, "ReportSystem.MB" + EvnPrefix.ToString() + "_ReportDayMonthYear.rdlc", False)
                    Case Else

                End Select
                'ShowClassReport(dtPicker1.Value, True, "ReportSystem.MachineBrand_ReportByClass.rdlc", False)

            Case "设备开动率"
                LoadReport_DevK1DS.ShowUptimeRateReport(FrmPtr.dtPicker1.Value, True, "ReportSystem.MB" + EvnPrefix.ToString() + "_ReportByClass.rdlc", True)
            Case "锭位故障率"

            Case "整机故障时统计"

            Case Else

        End Select
    End Sub

    Shared Sub ShowUptimeRateReport(dtFilter As Date, isDayClass As Boolean, _ReportEmbeddedResource As String, TestMode As Boolean)
        If TestMode Then
            dtFilter = FrmPtr.dtPicker1.Value
        End If

        Dim dta = New SYC_NSJ1DataSetTableAdapters.MachineBrand_GetOneDayClassYieldByDateTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim params() As ReportParameter
        If isDayClass Then
            rptALL.Name = "DataSet1"
            'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
            rptALL.Value = dta.GetData(dtFilter, 1)
            params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum"),
                 New ReportParameter("isDayClass", True)
                 }
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        Else
            rptALL.Name = "DataSet1"
            rptALL.Value = dta.GetData(dtFilter, 2)
            '如果改成晚班只需要 在此修改成 false
            params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum"),
                 New ReportParameter("isDayClass", False)
                 }
        End If

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If TestMode Then

        Else
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        End If

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()

    End Sub


    Shared Sub ShowDayMonthYear_PatternReport(dtFilter As Date, dateUnit As RptDateUnit, _ReportEmbeddedResource As String, TestMode As Boolean, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        If TestMode Then
            dtFilter = FrmPtr.dtPicker1.Value
        End If
        Dim dta = New SYC_NSJ1DataSetTableAdapters.a0Rpt_MachineBrandCore_GetYieldByDateTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim params() As ReportParameter
        rptALL.Name = "DataSet1"
        Select Case dateUnit
            Case RptDateUnit.Day
                Dim tmpdt = dta.GetDataByDateAndDateUnit(RptDateUnit.Day.ToString(), dtFilter, "")
                'TODO Linq 使用方法 
                'Dim tmp2 = (From item In tmpdt Where item.Hour > 10 And item.Hour <= 14 Select item).AsDataView.ToTable()
                rptALL.Value = tmpdt

                params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Day.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
                            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
            Case RptDateUnit.Month
                'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
                rptALL.Value = dta.GetDataByDateAndDateUnit(RptDateUnit.Month.ToString(), dtFilter, "")
                params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Month.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }

            Case RptDateUnit.Year
                rptALL.Value = dta.GetDataByDateAndDateUnit(RptDateUnit.Year.ToString(), dtFilter, "")
                params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Year.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
            Case RptDateUnit.TenYear
                rptALL.Value = dta.GetDataByDateAndDateUnit(RptDateUnit.TenYear.ToString(), dtFilter, "")
                params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.TenYear.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
            Case Else
                rptALL.Value = dta.GetDataByDateAndDateUnit(RptDateUnit.Day.ToString(), dtFilter, "")
                params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Day.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
        End Select

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()


    End Sub
    Shared Sub ShowClassReport(dtFilter As Date, isDayClass As Boolean, _ReportEmbeddedResource As String, TestMode As Boolean, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        If TestMode Then
            dtFilter = FrmPtr.dtPicker1.Value
        End If

        Dim dta = New SYC_NSJ1DataSetTableAdapters.MachineBrand_GetOneDayClassYieldByDateTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim params() As ReportParameter
        If isDayClass Then
            rptALL.Name = "DataSet1"
            'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）

            rptALL.Value = dta.GetData(dtFilter, 1)
            params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value),
                 New ReportParameter("AllYieldSum"),
                 New ReportParameter("isDayClass", True),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        Else
            rptALL.Name = "DataSet1"

            rptALL.Value = From item In dta.GetData(dtFilter, 2) Where item.LYieldSum > 100 Select item.Table




            '如果改成晚班只需要 在此修改成 false
            params = {
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum"),
                 New ReportParameter("isDayClass", False),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
        End If

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If TestMode Then

        Else
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        End If

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()

    End Sub
End Class
