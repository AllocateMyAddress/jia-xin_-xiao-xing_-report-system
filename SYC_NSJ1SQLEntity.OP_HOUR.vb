﻿Namespace SYC_NSJ1SQLEntity.OP_HOUR
    Public Class OP_HOUR
        ''' <summary>
        ''' RecordID
        ''' </summary>
        Public Property RecordID As Long

        ''' <summary>
        ''' MachineID
        ''' </summary>
        Public Property MachineID As Integer

        ''' <summary>
        ''' RecTime
        ''' </summary>
        Public Property RecTime As Date

        ''' <summary>
        ''' LYieldNum
        ''' </summary>
        Public Property LYieldNum As Integer

        ''' <summary>
        ''' RYieldNum
        ''' </summary>
        Public Property RYieldNum As Integer

        ''' <summary>
        ''' LDingSet
        ''' </summary>
        Public Property LDingSet As Integer

        ''' <summary>
        ''' RDingSet
        ''' </summary>
        Public Property RDingSet As Integer

        ''' <summary>
        ''' LSpeed
        ''' </summary>
        Public Property LSpeed As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' RSpeed
        ''' </summary>
        Public Property RSpeed As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' LTwist
        ''' </summary>
        Public Property LTwist As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' RTwist
        ''' </summary>
        Public Property RTwist As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' LDlex
        ''' </summary>
        Public Property LDlex As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' RDlex
        ''' </summary>
        Public Property RDlex As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' LHSet
        ''' </summary>
        Public Property LHSet As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' RHSet
        ''' </summary>
        Public Property RHSet As Global.System.Nullable(Of Integer)

        ''' <summary>
        ''' dTime
        ''' </summary>
        Public Property dTime As String

        ''' <summary>
        ''' mTime
        ''' </summary>
        Public Property mTime As String

        ''' <summary>
        ''' yTime
        ''' </summary>
        Public Property yTime As String

    End Class
End Namespace