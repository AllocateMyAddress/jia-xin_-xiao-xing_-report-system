﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.dtPicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbbxRptType = New System.Windows.Forms.ComboBox()
        Me.lblCBBXRptType = New System.Windows.Forms.Label()
        Me.cbxEnableDateRange = New System.Windows.Forms.CheckBox()
        Me.dtPicker2 = New System.Windows.Forms.DateTimePicker()
        Me.lblDtp2 = New System.Windows.Forms.Label()
        Me.cbbxCycle = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UITEXT_Brand = New System.Windows.Forms.Label()
        Me.cbbxBrand = New System.Windows.Forms.ComboBox()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.UIText_SqlServerConnStatus = New System.Windows.Forms.Label()
        Me.btnQuery = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.UI_TextSQLSvr = New System.Windows.Forms.Label()
        Me.BtnCanel = New System.Windows.Forms.Button()
        Me.cbxTestMode = New System.Windows.Forms.CheckBox()
        Me.flowPanelFliters = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Pbx1 = New System.Windows.Forms.PictureBox()
        Me.BtnTestAsyncQuery = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.flowPanelFliters.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.Pbx1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ReportViewer1.AutoSize = True
        Me.ReportViewer1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.MachineTypeDayReoprt.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 3)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.PromptAreaCollapsed = True
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.ShowBackButton = False
        Me.ReportViewer1.ShowContextMenu = False
        Me.ReportViewer1.ShowFindControls = False
        Me.ReportViewer1.ShowPageNavigationControls = False
        Me.ReportViewer1.ShowPrintButton = False
        Me.ReportViewer1.Size = New System.Drawing.Size(1599, 789)
        Me.ReportViewer1.TabIndex = 0
        '
        'dtPicker1
        '
        Me.dtPicker1.CustomFormat = "yyyy/MM/dd"
        Me.dtPicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPicker1.Location = New System.Drawing.Point(86, 5)
        Me.dtPicker1.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.dtPicker1.Name = "dtPicker1"
        Me.dtPicker1.Size = New System.Drawing.Size(136, 21)
        Me.dtPicker1.TabIndex = 1
        Me.dtPicker1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 8)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 12)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "筛选·日期："
        Me.Label1.Visible = False
        '
        'cbbxRptType
        '
        Me.cbbxRptType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbxRptType.Font = New System.Drawing.Font("微软雅黑", 9.0!)
        Me.cbbxRptType.Items.AddRange(New Object() {"设备生产量", "设备开动率", "锭位故障率", "整机故障时长统计"})
        Me.cbbxRptType.Location = New System.Drawing.Point(79, 4)
        Me.cbbxRptType.Name = "cbbxRptType"
        Me.cbbxRptType.Size = New System.Drawing.Size(149, 25)
        Me.cbbxRptType.TabIndex = 3
        '
        'lblCBBXRptType
        '
        Me.lblCBBXRptType.AutoSize = True
        Me.lblCBBXRptType.Location = New System.Drawing.Point(12, 11)
        Me.lblCBBXRptType.Name = "lblCBBXRptType"
        Me.lblCBBXRptType.Size = New System.Drawing.Size(65, 12)
        Me.lblCBBXRptType.TabIndex = 2
        Me.lblCBBXRptType.Text = "报表类别："
        '
        'cbxEnableDateRange
        '
        Me.cbxEnableDateRange.AutoSize = True
        Me.cbxEnableDateRange.Location = New System.Drawing.Point(679, 8)
        Me.cbxEnableDateRange.Margin = New System.Windows.Forms.Padding(3, 8, 3, 3)
        Me.cbxEnableDateRange.Name = "cbxEnableDateRange"
        Me.cbxEnableDateRange.Size = New System.Drawing.Size(96, 16)
        Me.cbxEnableDateRange.TabIndex = 4
        Me.cbxEnableDateRange.Text = "启用日期区间"
        Me.cbxEnableDateRange.UseVisualStyleBackColor = True
        Me.cbxEnableDateRange.Visible = False
        '
        'dtPicker2
        '
        Me.dtPicker2.CustomFormat = "yyyy/MM/dd"
        Me.dtPicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPicker2.Location = New System.Drawing.Point(251, 5)
        Me.dtPicker2.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.dtPicker2.Name = "dtPicker2"
        Me.dtPicker2.Size = New System.Drawing.Size(136, 21)
        Me.dtPicker2.TabIndex = 1
        '
        'lblDtp2
        '
        Me.lblDtp2.AutoSize = True
        Me.lblDtp2.Font = New System.Drawing.Font("微软雅黑", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDtp2.Location = New System.Drawing.Point(228, 7)
        Me.lblDtp2.Margin = New System.Windows.Forms.Padding(3, 7, 3, 0)
        Me.lblDtp2.Name = "lblDtp2"
        Me.lblDtp2.Size = New System.Drawing.Size(17, 17)
        Me.lblDtp2.TabIndex = 2
        Me.lblDtp2.Text = "~"
        Me.lblDtp2.Visible = False
        '
        'cbbxCycle
        '
        Me.cbbxCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbxCycle.Font = New System.Drawing.Font("微软雅黑", 9.0!)
        Me.cbbxCycle.Items.AddRange(New Object() {"班报", "日报", "月报"})
        Me.cbbxCycle.Location = New System.Drawing.Point(440, 3)
        Me.cbbxCycle.Name = "cbbxCycle"
        Me.cbbxCycle.Size = New System.Drawing.Size(90, 25)
        Me.cbbxCycle.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(393, 8)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 12)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "周期："
        Me.Label2.Visible = False
        '
        'UITEXT_Brand
        '
        Me.UITEXT_Brand.AutoSize = True
        Me.UITEXT_Brand.Location = New System.Drawing.Point(536, 8)
        Me.UITEXT_Brand.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.UITEXT_Brand.Name = "UITEXT_Brand"
        Me.UITEXT_Brand.Size = New System.Drawing.Size(41, 12)
        Me.UITEXT_Brand.TabIndex = 2
        Me.UITEXT_Brand.Text = "品牌："
        Me.UITEXT_Brand.Visible = False
        '
        'cbbxBrand
        '
        Me.cbbxBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbxBrand.Font = New System.Drawing.Font("微软雅黑", 9.0!)
        Me.cbbxBrand.Items.AddRange(New Object() {"*", "新Allma", "Allma", "AllmaCC4", "宜昌", "天竺"})
        Me.cbbxBrand.Location = New System.Drawing.Point(583, 3)
        Me.cbbxBrand.Name = "cbbxBrand"
        Me.cbbxBrand.Size = New System.Drawing.Size(90, 25)
        Me.cbbxBrand.TabIndex = 3
        Me.cbbxBrand.Visible = False
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(150, 150)
        '
        'UIText_SqlServerConnStatus
        '
        Me.UIText_SqlServerConnStatus.AutoSize = True
        Me.UIText_SqlServerConnStatus.Font = New System.Drawing.Font("微软雅黑", 10.0!)
        Me.UIText_SqlServerConnStatus.Location = New System.Drawing.Point(314, 0)
        Me.UIText_SqlServerConnStatus.Name = "UIText_SqlServerConnStatus"
        Me.UIText_SqlServerConnStatus.Size = New System.Drawing.Size(65, 20)
        Me.UIText_SqlServerConnStatus.TabIndex = 5
        Me.UIText_SqlServerConnStatus.Text = "链接状态"
        '
        'btnQuery
        '
        Me.btnQuery.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnQuery.BackColor = System.Drawing.Color.LimeGreen
        Me.btnQuery.Font = New System.Drawing.Font("微软雅黑", 22.0!)
        Me.btnQuery.Location = New System.Drawing.Point(7, 3)
        Me.btnQuery.MaximumSize = New System.Drawing.Size(122, 60)
        Me.btnQuery.Name = "btnQuery"
        Me.btnQuery.Size = New System.Drawing.Size(105, 57)
        Me.btnQuery.TabIndex = 6
        Me.btnQuery.Text = "查询"
        Me.btnQuery.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.UIText_SqlServerConnStatus)
        Me.FlowLayoutPanel1.Controls.Add(Me.UI_TextSQLSvr)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnCanel)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnQuery)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(1218, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(382, 63)
        Me.FlowLayoutPanel1.TabIndex = 7
        '
        'UI_TextSQLSvr
        '
        Me.UI_TextSQLSvr.AutoSize = True
        Me.UI_TextSQLSvr.Font = New System.Drawing.Font("微软雅黑", 10.0!)
        Me.UI_TextSQLSvr.ForeColor = System.Drawing.Color.Blue
        Me.UI_TextSQLSvr.Location = New System.Drawing.Point(224, 0)
        Me.UI_TextSQLSvr.Name = "UI_TextSQLSvr"
        Me.UI_TextSQLSvr.Size = New System.Drawing.Size(84, 20)
        Me.UI_TextSQLSvr.TabIndex = 7
        Me.UI_TextSQLSvr.Text = ": SQL服务器"
        Me.UI_TextSQLSvr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnCanel
        '
        Me.BtnCanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnCanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BtnCanel.Font = New System.Drawing.Font("微软雅黑", 22.0!)
        Me.BtnCanel.Location = New System.Drawing.Point(118, 3)
        Me.BtnCanel.MaximumSize = New System.Drawing.Size(122, 60)
        Me.BtnCanel.Name = "BtnCanel"
        Me.BtnCanel.Size = New System.Drawing.Size(100, 57)
        Me.BtnCanel.TabIndex = 11
        Me.BtnCanel.Text = "取消"
        Me.BtnCanel.UseVisualStyleBackColor = False
        '
        'cbxTestMode
        '
        Me.cbxTestMode.AutoSize = True
        Me.cbxTestMode.Location = New System.Drawing.Point(250, 10)
        Me.cbxTestMode.Name = "cbxTestMode"
        Me.cbxTestMode.Size = New System.Drawing.Size(72, 16)
        Me.cbxTestMode.TabIndex = 4
        Me.cbxTestMode.Text = "测试模式"
        Me.cbxTestMode.UseVisualStyleBackColor = True
        Me.cbxTestMode.Visible = False
        '
        'flowPanelFliters
        '
        Me.flowPanelFliters.AutoSize = True
        Me.flowPanelFliters.Controls.Add(Me.Label1)
        Me.flowPanelFliters.Controls.Add(Me.dtPicker1)
        Me.flowPanelFliters.Controls.Add(Me.lblDtp2)
        Me.flowPanelFliters.Controls.Add(Me.dtPicker2)
        Me.flowPanelFliters.Controls.Add(Me.Label2)
        Me.flowPanelFliters.Controls.Add(Me.cbbxCycle)
        Me.flowPanelFliters.Controls.Add(Me.UITEXT_Brand)
        Me.flowPanelFliters.Controls.Add(Me.cbbxBrand)
        Me.flowPanelFliters.Controls.Add(Me.cbxEnableDateRange)
        Me.flowPanelFliters.Location = New System.Drawing.Point(1, 35)
        Me.flowPanelFliters.Name = "flowPanelFliters"
        Me.flowPanelFliters.Size = New System.Drawing.Size(793, 31)
        Me.flowPanelFliters.TabIndex = 8
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.Pbx1)
        Me.Panel1.Controls.Add(Me.ReportViewer1)
        Me.Panel1.Location = New System.Drawing.Point(1, 69)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1599, 792)
        Me.Panel1.TabIndex = 10
        '
        'Pbx1
        '
        Me.Pbx1.Location = New System.Drawing.Point(679, 288)
        Me.Pbx1.Name = "Pbx1"
        Me.Pbx1.Size = New System.Drawing.Size(200, 200)
        Me.Pbx1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Pbx1.TabIndex = 1
        Me.Pbx1.TabStop = False
        Me.Pbx1.Visible = False
        '
        'BtnTestAsyncQuery
        '
        Me.BtnTestAsyncQuery.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnTestAsyncQuery.BackColor = System.Drawing.Color.Pink
        Me.BtnTestAsyncQuery.Font = New System.Drawing.Font("微软雅黑", 22.0!)
        Me.BtnTestAsyncQuery.Location = New System.Drawing.Point(1022, 6)
        Me.BtnTestAsyncQuery.Name = "BtnTestAsyncQuery"
        Me.BtnTestAsyncQuery.Size = New System.Drawing.Size(159, 57)
        Me.BtnTestAsyncQuery.TabIndex = 6
        Me.BtnTestAsyncQuery.Text = "测试并发查询"
        Me.BtnTestAsyncQuery.UseVisualStyleBackColor = False
        Me.BtnTestAsyncQuery.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1602, 861)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.BtnTestAsyncQuery)
        Me.Controls.Add(Me.flowPanelFliters)
        Me.Controls.Add(Me.cbxTestMode)
        Me.Controls.Add(Me.cbbxRptType)
        Me.Controls.Add(Me.lblCBBXRptType)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(1250, 475)
        Me.Name = "Form1"
        Me.Text = "报表查看器 Ver 0.0.1"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.flowPanelFliters.ResumeLayout(False)
        Me.flowPanelFliters.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Pbx1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub


    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents dtPicker1 As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents cbbxRptType As ComboBox
    Friend WithEvents lblCBBXRptType As Label
    Friend WithEvents cbxEnableDateRange As CheckBox
    Friend WithEvents dtPicker2 As DateTimePicker
    Friend WithEvents lblDtp2 As Label
    Friend WithEvents cbbxCycle As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents UITEXT_Brand As Label
    Friend WithEvents cbbxBrand As ComboBox
    Friend WithEvents BottomToolStripPanel As ToolStripPanel
    Friend WithEvents TopToolStripPanel As ToolStripPanel
    Friend WithEvents RightToolStripPanel As ToolStripPanel
    Friend WithEvents LeftToolStripPanel As ToolStripPanel
    Friend WithEvents ContentPanel As ToolStripContentPanel
    Friend WithEvents UIText_SqlServerConnStatus As Label
    Friend WithEvents btnQuery As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents UI_TextSQLSvr As Label
    Public WithEvents cbxTestMode As CheckBox
    Friend WithEvents flowPanelFliters As FlowLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BtnCanel As Button
    Friend WithEvents BtnTestAsyncQuery As Button
    Friend WithEvents Pbx1 As PictureBox
End Class
