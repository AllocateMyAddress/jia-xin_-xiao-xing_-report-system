﻿Imports Microsoft.Reporting.WinForms
Imports System.Linq
Imports System.Runtime.CompilerServices

Class LoadReport

    ''' <summary>
    ''' 聚合等级，如果有Sum 或Count的话就会累加到 对应的级别。
    ''' </summary>
    Enum AggLevel
        Hour
        ShiftID
        Day
        Month
        Year
        TenYear
    End Enum

    Public Shared FrmPtr As Form1

    Shared DoOnceFlag As Boolean = True

    Public Shared Sub DoOnce(func As Action)
        If DoOnceFlag Then
            func()
            DoOnceFlag = False
        End If
    End Sub

    '为了好管理 写在 这边，这方法会在Form时初始化。 参考了 Unity3D的 框架设计, 增加了 可维护性,与可读性 与可管理性
    Shared Sub InitRptTypeByMainForm1()

        FrmPtr.cbbxRptType.Items.Clear()
        FrmPtr.cbbxRptType.Items.AddRange({"测试1设备生产量", "测试2设备开动率", "测试3锭位故障率", "测试4整机故障报表"})

        '追加 需要扩展的语句， 不过要执行一次后才能
    End Sub


    Shared Sub InitRptCycleByMainForm1()

        If FrmPtr.cbxTestMode.Checked Then
            '执行一个 测试模式下需要用到的 报表选项。
            FrmPtr.InitPhase_LoadCbbxRptType(FrmPtr, New Action(Of Form1)(Sub(x)
                                                                              Select Case x.cbbxRptType.SelectedItem?.ToString()
                                                                                  Case "测试1设备生产量"
                                                                                      Normal1(x)
                                                                                  Case "测试2设备开动率"
                                                                                      Normal1(x)
                                                                                  Case "测试3锭位故障率"
                                                                                      Normal1(x)
                                                                                  Case "测试4整机故障报表"
                                                                                      Normal1(x)
                                                                              End Select
                                                                          End Sub))
        Else
            '执行一个 空的 方法。
            FrmPtr.InitPhase_LoadCbbxRptType(FrmPtr, New Action(Of Form1)(Sub(x)
                                                                          End Sub))
        End If


    End Sub


    Private Shared Function Normal1(x As Form1)
        x.cbbxCycle.Items.Clear()
        x.cbbxCycle.Items.AddRange({"日报", "月报", "年报"})
        Application.DoEvents()
        x.cbbxCycle.SelectedIndex = 0
        x.UITextBrandShow(True)
        Return x
    End Function





    Shared Sub GetReport(EnvPrefix As DefEnvConfig)
        FrmPtr.ISLoadingReport = True
        ComonFuncs.DelaySync(100)

        ' 记得修改 二科 对应的 报表 载入方法。
        Dim DeptStr = ""
        Select Case EnvPrefix
            Case DefEnvConfig.k1
                DeptStr = "一科"

            Case DefEnvConfig.k2
                DeptStr = "二科"
            Case Else

        End Select

        Dim DateUnit = RptDateUnit.Day
        Select Case FrmPtr.cbbxCycle.SelectedItem
            Case "日报"
                DateUnit = RptDateUnit.Day
            Case "月报"
                DateUnit = RptDateUnit.Month
            Case "年报"
                DateUnit = RptDateUnit.Year
            Case "十年报"
                DateUnit = RptDateUnit.TenYear
            Case "自选区间"
                DateUnit = RptDateUnit.DateRange
        End Select
        '  CommonParams 区域
        Dim CommonParams As List(Of ReportParameter) = New List(Of ReportParameter)(5)

        If DateUnit = RptDateUnit.Day Then
            CommonParams.AddRange(New ReportParameter() {
                                  New ReportParameter("DeptStr", DeptStr),
                                  New ReportParameter("RptCycle", DateUnit.ToString()),
                                  New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月dd日"))
                                  })
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        ElseIf DateUnit = RptDateUnit.Month Then
            CommonParams.AddRange(New ReportParameter() {
                                  New ReportParameter("DeptStr", DeptStr),
                                  New ReportParameter("RptCycle", DateUnit.ToString()),
                                  New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月"))
                                  })

            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        ElseIf DateUnit = RptDateUnit.Year Then
            CommonParams.AddRange(New ReportParameter() {
                                  New ReportParameter("DeptStr", DeptStr),
                                  New ReportParameter("RptCycle", DateUnit.ToString()),
                                  New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年"))
                                  })
        Else
            MessageBox.Show("报表周期单位错误; CommonParams 区域")
            Return
        End If


        Try
            Select Case FrmPtr.cbbxRptType.SelectedItem?.ToString()
            '------测试用的
            'Case "机型班报"
            '    ShowClassReportK1(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报.rdlc")
            'Case "机型日报"


                Case "测试1设备生产量"
                    Select Case FrmPtr.cbbxCycle.SelectedItem
                        Case "日报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowYieldReportK1FromOP_HourOldVersion(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc", YieldUnit.Ton)
                                'Test_ShowClassReportK1FromOPClass(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc", YieldUnit.Ton)
                            Else
                                ShowYieldReportK2FromOP_HourOldVersion(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc", YieldUnit.Ton)
                            End If

                    '--------------------------------------------------------------------------------------
                        Case "月报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowYieldReportK1FromOP_HourOldVersion(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报.rdlc", YieldUnit.Ton)
                            Else
                                ShowYieldReportK2FromOP_HourOldVersion(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报.rdlc", YieldUnit.Ton)
                            End If

                    '--------------------------------------------------------------------------------------
                        Case "年报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowYieldReportK1FromOP_HourOldVersion(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量年报.rdlc", YieldUnit.Ton)
                            Else
                                ShowYieldReportK2FromOP_HourOldVersion(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量年报.rdlc", YieldUnit.Ton)
                            End If
                        Case Else

                    End Select

                Case "设备生产量"
                    Select Case FrmPtr.cbbxCycle.SelectedItem
                        Case "日报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                'ShowClassReportK1(FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc", YieldUnit.Metre)

                                ShowYield_RptK1FromOP_Hour_V2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc")
                                'ShowYieldReportK1FromOp_Class(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc", YieldUnit.Metre)
                            Else
                                ShowYield_RptK2FromOP_Hour_V2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc")
                                'ShowYieldReportK2FromOp_Class(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量日报v1.rdlc", YieldUnit.Metre)
                            End If


                    '--------------------------------------------------------------------------------------
                        Case "月报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ' 月报只是时间单位 不一样， 调用同一个 存储过程，通过报表自动聚合成 对应的时间单位。
                                'ShowYieldReportK1FromOp_Class(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc", YieldUnit.Ton)
                                'Test_ShowYieldReportK1FromOp_ClassByV3Proc(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc")
                                ShowYield_RptK1FromOP_Hour_V2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc")
                            Else
                                'ShowYieldReportK2FromOp_Class(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc", YieldUnit.Ton)
                                ShowYield_RptK2FromOP_Hour_V2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc")

                                'While True
                                '    If Tmp1?.Count >= 0 Or Tmp1 Is Nothing Then

                                '        MessageBox.Show("读取完成")
                                '        Exit While
                                '    End If
                                'End While
                            End If

                    '--------------------------------------------------------------------------------------
                        Case "年报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                'ShowYieldReportK1FromOp_Class(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc", YieldUnit.Ton)
                                'Test_ShowYieldReportK1FromOp_ClassByV3Proc(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc")
                                ShowYield_RptK1FromOP_Hour_V2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc")
                            Else
                                'ShowYieldReportK2FromOp_Class(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc", YieldUnit.Ton)
                                ShowYield_RptK2FromOP_Hour_V2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_产量月报年报v1_吨.rdlc")
                            End If
                        Case Else

                    End Select
                'ShowClassReport(dtPicker1.Value, True, "ReportSystem.MachineBrand_ReportByClass.rdlc", False)

                Case "设备开动率"
                    Select Case FrmPtr.cbbxCycle.SelectedItem
                        Case "日报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowUptimeRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MB" + EnvPrefix.ToString() + "_开动率_日报.rdlc")
                            Else
                                ShowUptimeRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MB" + EnvPrefix.ToString() + "_开动率_日报.rdlc")

                            End If

                        Case "月报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowUptimeRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MB" + EnvPrefix.ToString() + "_开动率_月报.rdlc")
                            Else
                                ShowUptimeRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MB" + EnvPrefix.ToString() + "_开动率_月报.rdlc")
                            End If

                        Case "年报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowUptimeRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MB" + EnvPrefix.ToString() + "_开动率_年报.rdlc")
                            Else
                                ShowUptimeRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MB" + EnvPrefix.ToString() + "_开动率_年报.rdlc")
                            End If

                        Case Else

                    End Select

                Case "锭位故障率"
                    Select Case FrmPtr.cbbxCycle.SelectedItem
                        Case "日报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowAlertRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_定位故障率.rdlc")
                            Else
                                ShowAlertRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Day, "ReportSystem.MID" + EnvPrefix.ToString() + "_定位故障率.rdlc")
                            End If


                        Case "月报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowAlertRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_定位故障率_月报.rdlc")
                            Else
                                ShowAlertRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.MID" + EnvPrefix.ToString() + "_定位故障率_月报.rdlc")
                            End If

                        Case "年报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowAlertRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_定位故障率_年报.rdlc")
                            Else
                                ShowAlertRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.MID" + EnvPrefix.ToString() + "_定位故障率_年报.rdlc")
                            End If

                        Case Else

                    End Select

                Case "整机故障时长统计"
                    Select Case FrmPtr.cbbxCycle.SelectedItem
                        Case "月报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowAllMachineAlertRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.NA" + EnvPrefix.ToString() + "_整机故障.rdlc")
                            Else
                                Dim Tmp1 = ShowAllMachineAlertRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Month, "ReportSystem.NA" + EnvPrefix.ToString() + "_整机故障.rdlc")


                            End If

                        Case "年报"
                            If EnvPrefix = DefEnvConfig.k1 Then
                                ShowAllMachineAlertRateReportK1(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.NA" + EnvPrefix.ToString() + "_整机故障.rdlc")
                            Else
                                ShowAllMachineAlertRateReportK2(CommonParams, FrmPtr.dtPicker1.Value, DeptStr, RptDateUnit.Year, "ReportSystem.NA" + EnvPrefix.ToString() + "_整机故障.rdlc")
                            End If
                        Case Else

                    End Select

            End Select

        Catch ex As Exception

            Throw
        End Try

        FrmPtr.ISLoadingReport = False
        Application.DoEvents()

    End Sub


    Shared Sub ShowAllMachineAlertRateReportK1(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String)

        Dim dta = New DS_1keTableAdapters._v1_Rpt_GetWholeAlarmByDate_FromMacAlarmWholeTableAdapter()
        Dim rptALL As New ReportDataSource

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        Dim QueryRet = New DS_1ke._v1_Rpt_GetWholeAlarmByDate_FromMacAlarmWholeDataTable()
        Dim RowList = New List(Of DS_1ke._v1_Rpt_GetWholeAlarmByDate_FromMacAlarmWholeRow)()

        rptALL.Name = "DataSet1"

        '日期区间实现区
        'If FrmPtr.cbxEnableDateRange.Checked Then
        '    QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter)
        'Else
        '    QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        'End If
        '因为存储过程 的设计 没有区间可选,因此覆盖掉上边的
        QueryRet = dta.GetData(DateUnit.ToString(), FrmPtr.dtPicker1.Value, FrmPtr.dtPicker2.Value)
        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            'RowList = (From item In QueryRet Where ((FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" AndAlso item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper() And (item.MachineID < 6 Or item.MachineID > 10)) _
            '                           OrElse (item.MachineID < 6 Or item.MachineID > 10)) Select item).ToList()

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item)

                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item)
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item)
            Else

            End If
        End If

        rptALL.Value = RowList


        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()

    End Sub

    Shared Function ShowAllMachineAlertRateReportK2(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String)

        Dim dta = New DS_K2TableAdapters._v1_Rpt_GetWholeAlarmByDate_FromMacAlarmWholeTableAdapter()
        Dim rptALL As New ReportDataSource

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        Dim QueryRet = New DS_K2._v1_Rpt_GetWholeAlarmByDate_FromMacAlarmWholeDataTable()
        Dim RowList = New List(Of DS_K2._v1_Rpt_GetWholeAlarmByDate_FromMacAlarmWholeRow)()

        rptALL.Name = "DataSet1"

        '日期区间实现区
        'If FrmPtr.cbxEnableDateRange.Checked Then
        '    QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter)
        'Else
        '    QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        'End If
        '因为存储过程 的设计 没有区间可选,因此覆盖掉上边的
        QueryRet = dta.GetData(DateUnit.ToString(), FrmPtr.dtPicker1.Value, FrmPtr.dtPicker2.Value)
        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            'RowList = (From item In QueryRet Where ((FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" AndAlso item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper() And (item.MachineID < 6 Or item.MachineID > 10)) _
            '                           OrElse (item.MachineID < 6 Or item.MachineID > 10)) Select item).ToList()

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item)

                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item)
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item)
            Else

            End If
        End If

        rptALL.Value = RowList
        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
        Return Nothing

    End Function


    Shared Sub ShowAlertRateReportK1(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String)
        Dim dta = New DS_1keTableAdapters._v1_GetAlarmRate_FromMACAlarmPercentTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim QueryRet = New DS_1ke._v1_GetAlarmRate_FromMACAlarmPercentDataTable()
        Dim RowList = New List(Of DS_1ke._v1_GetAlarmRate_FromMACAlarmPercentRow)()
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptALL.Name = "DataSet1"

        '日期区间实现区
        'If FrmPtr.cbxEnableDateRange.Checked Then
        '    QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter)
        'Else
        '    QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        'End If
        '因为存储过程 的设计 没有区间可选,因此覆盖掉上边的
        QueryRet = dta.GetData(FrmPtr.dtPicker1.Value, DateUnit.ToString())
        RowList = QueryRet.ToList()

        If FrmPtr.SkipK1MIDEnable Then

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then

                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else

                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptALL.Value = RowList

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()

    End Sub

    Shared Sub ShowAlertRateReportK2(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String)
        Dim dta = New DS_K2TableAdapters._v1_GetAlarmRate_FromMACAlarmPercentTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim QueryRet = New DS_K2._v1_GetAlarmRate_FromMACAlarmPercentDataTable()
        Dim RowList = New List(Of DS_K2._v1_GetAlarmRate_FromMACAlarmPercentRow)()
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptALL.Name = "DataSet1"

        '日期区间实现区
        'If FrmPtr.cbxEnableDateRange.Checked Then
        '    QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter)
        'Else
        '    QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        'End If
        '因为存储过程 的设计 没有区间可选,因此覆盖掉上边的
        QueryRet = dta.GetData(FrmPtr.dtPicker1.Value, DateUnit.ToString())
        RowList = QueryRet.ToList()

        If FrmPtr.SkipK1MIDEnable Then

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then

                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else

                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If
        rptALL.Value = RowList

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()

    End Sub



    Shared Sub ShowUptimeRateReportK1(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String)
        Dim dta = New DS_1keTableAdapters._GetAlarmStatisticsDataFromMacAlarmPercentTableAdapter()
        Dim rptALL As New ReportDataSource
        Dim QueryRet = New DS_1ke._GetAlarmStatisticsDataFromMacAlarmPercentDataTable()
        Dim RowList = New List(Of DS_1ke._GetAlarmStatisticsDataFromMacAlarmPercentRow)()
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptALL.Name = "DataSet1"

        '日期区间实现区
        'If FrmPtr.cbxEnableDateRange.Checked Then
        '    QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter)
        'Else
        '    QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        'End If
        '因为存储过程 的设计 没有区间可选,因此覆盖掉上边的
        QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            'RowList = (From item In QueryRet Where ((FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" AndAlso item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper() And (item.MachineID < 6 Or item.MachineID > 10)) _
            '                           OrElse (item.MachineID < 6 Or item.MachineID > 10)) Select item).ToList()

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else

                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else

            End If
        End If

        Dim RunRate1 = IIf(RowList.Count = 0, 0, (From item In RowList Select item.RunRate).Sum() / (From item In RowList Select item.MachineID).Count())
        rptALL.Value = RowList
        params.AddRange(New ReportParameter() {
                        New ReportParameter("SumUptimeRate", CStr(RunRate1))
                 })

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        Else
            FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
            FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        End If
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub

    Shared Sub ShowUptimeRateReportK2(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String)
        Dim dta = New DS_K2TableAdapters._GetAlarmStatisticsDataFromMacAlarmPercentTableAdapter()
        Dim rptALL As New ReportDataSource
        Dim QueryRet = New DS_K2._GetAlarmStatisticsDataFromMacAlarmPercentDataTable
        Dim RowList = New List(Of DS_K2._GetAlarmStatisticsDataFromMacAlarmPercentRow)()
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptALL.Name = "DataSet1"

        '日期区间实现区
        'If FrmPtr.cbxEnableDateRange.Checked Then
        '    QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter)
        'Else
        '    QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        'End If
        '因为存储过程 的设计 没有区间可选,因此覆盖掉上边的
        QueryRet = dta.GetData(DateUnit.ToString(), dtFilter)
        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            'RowList = (From item In QueryRet Where ((FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" AndAlso item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper() And (item.MachineID < 6 Or item.MachineID > 10)) _
            '                           OrElse (item.MachineID < 6 Or item.MachineID > 10)) Select item).ToList()

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else

                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else

            End If
        End If

        Dim RunRate1 = IIf(RowList.Count = 0, 0, (From item In RowList Select item.RunRate).Sum() / (From item In RowList Select item.MachineID).Count())

        rptALL.Value = RowList

        params.AddRange(New ReportParameter() {
                        New ReportParameter("SumUptimeRate", CStr(RunRate1))
                 })

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        Else
            FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
            FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        End If


        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub


    Shared Sub ShowDayMonthYear_PatternReportOld(dtFilter As Date, DeptStr As String, dateUnit As RptDateUnit, _ReportEmbeddedResource As String, TestMode As Boolean, Optional yieldUnit As YieldUnit = YieldUnit.Metre)
        If TestMode Then
            dtFilter = FrmPtr.dtPicker1.Value
        End If
        Dim dta = New DS_K2TableAdapters._v1_Rpt_GetYieldByDate_FromOP_HourTableAdapter()
        Dim rptALL As New ReportDataSource

        Dim params() As ReportParameter
        rptALL.Name = "DataSet1"
        Select Case dateUnit
            Case RptDateUnit.Day
                'TODO Linq 使用方法 
                'Dim tmp2 = (From item In tmpdt Where item.Hour > 10 And item.Hour <= 14 Select item).AsDataView.ToTable()
                Dim tmpObj = dta.GetData(RptDateUnit.Month.ToString(), dtFilter, Nothing)
                rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()

                params = {
                     New ReportParameter("DeptStr", DeptStr),
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Day.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
                            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
            Case RptDateUnit.Month
                'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
                Dim tmpObj = dta.GetData(RptDateUnit.Month.ToString(), dtFilter, Nothing)
                rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()
                params = {
                     New ReportParameter("DeptStr", DeptStr),
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月")),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Month.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }

            Case RptDateUnit.Year
                Dim tmpObj = dta.GetData(RptDateUnit.Year.ToString(), dtFilter, Nothing)
                rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()
                params = {
                     New ReportParameter("DeptStr", DeptStr),
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年")),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Year.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
            Case RptDateUnit.TenYear
                Dim tmpObj = dta.GetData(RptDateUnit.TenYear.ToString(), dtFilter, Nothing)
                rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()
                params = {
                     New ReportParameter("DeptStr", DeptStr),
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年") + " ~ " + FrmPtr.dtPicker1.Value.AddYears(10).ToString("yyyy年")),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.TenYear.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
            Case Else
                Dim tmpObj = dta.GetData(RptDateUnit.Day.ToString(), dtFilter, Nothing)
                rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()
                params = {
                     New ReportParameter("DeptStr", DeptStr),
                 New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToLongDateString()),
                 New ReportParameter("AllYieldSum", 0),
                 New ReportParameter("RptCycle", RptDateUnit.Day.ToString()),
                 New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                 }
        End Select

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()


    End Sub


    Shared Sub ShowClassReportK1FromOPClass(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v1_Rpt_FromOP_ClassTableAdapter()

        Dim rptDS1 As New ReportDataSource
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptDS1.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        Dim QueryRet = New DS_1ke._v1_Rpt_FromOP_ClassDataTable
        Dim RowList = New List(Of DS_1ke._v1_Rpt_FromOP_ClassRow)()
        '日期区间实现区
        If FrmPtr.cbxEnableDateRange.Checked Then
            QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
        Else
            QueryRet = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
        End If

        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else
                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If
        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptDS1.Value = RowList

        params.AddRange(New ReportParameter() {
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram", yieldUnit = YieldUnit.Ton, "ton"))))
                     })
        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptDS1)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
            Return
        End If
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub

    Shared Sub ShowClassReportK2FromOPClass(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v1_Rpt_FromOP_ClassTableAdapter()

        Dim rptDS1 As New ReportDataSource
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptDS1.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        Dim QueryRet = New DS_1ke._v1_Rpt_FromOP_ClassDataTable
        Dim RowList = New List(Of DS_1ke._v1_Rpt_FromOP_ClassRow)()
        '日期区间实现区
        If FrmPtr.cbxEnableDateRange.Checked Then
            QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
        Else
            QueryRet = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
        End If

        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else
                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If
        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptDS1.Value = RowList

        params.AddRange(New ReportParameter() {
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram", yieldUnit = YieldUnit.Ton, "ton"))))
                     })
        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptDS1)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
            Return
        End If
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub

    Shared Sub Test_ShowClassReportK1FromOPClass(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v1_Rpt_FromOP_ClassTableAdapter()
        Dim rptDS1 As New ReportDataSource
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptDS1.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        Dim QueryRet = New DS_1ke._v1_Rpt_FromOP_ClassDataTable
        Dim RowList = New List(Of DS_1ke._v1_Rpt_FromOP_ClassRow)()
        '日期区间实现区
        If FrmPtr.cbxEnableDateRange.Checked Then
            QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
        Else
            QueryRet = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
        End If

        RowList = QueryRet.ToList()
        If FrmPtr.SkipK1MIDEnable Then
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else
                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If
        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptDS1.Value = RowList

        params.AddRange(New ReportParameter() {
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram", yieldUnit = YieldUnit.Ton, "ton"))))
                     })
        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If

        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptDS1)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
            Return
        End If
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params"></param>
    ''' <param name="dtFilter"></param>
    ''' <param name="DeptStr"></param>
    ''' <param name="RptDateUnit"></param>
    ''' <param name="_ReportEmbeddedResource"></param>
    Shared Sub ShowYield_RptK1FromOP_Hour_V2(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, RptDateUnit As RptDateUnit, _ReportEmbeddedResource As String)

        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v2_Rpt_FromOP_ClassTableAdapter()

        Dim rptALL = New ReportDataSource
        rptALL.Name = "DataSet1"

        Dim QueryRet = New DS_1ke._v2_Rpt_FromOP_ClassDataTable()
        Dim RowList = New List(Of DS_1ke._v2_Rpt_FromOP_ClassRow)()
        RowList = dta.GetData(RptDateUnit.ToString(), dtFilter, Nothing, False, Nothing).ToList()
        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        Else
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
            'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
            If FrmPtr.cbxEnableDateRange.Checked Then
                RowList = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2, True, AggLevel.Day.ToString()).ToList
            Else
                RowList = dta.GetData(RptDateUnit.ToString(), dtFilter, Nothing, False, Nothing).ToList
            End If
            If FrmPtr.SkipK1MIDEnable Then

                If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                    RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
                Else
                    RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
                End If

            Else
                If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                    RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
                End If
            End If
            params.Add(New ReportParameter("YieldUnit", YieldUnit.Ton.ToString))

        End If
        FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        rptALL.Value = RowList
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub

    Shared Sub ShowYield_RptK2FromOP_Hour_V2(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, RptDateUnit As RptDateUnit, _ReportEmbeddedResource As String)

        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_K2TableAdapters._v2_Rpt_FromOP_ClassTableAdapter()

        Dim rptALL = New ReportDataSource
        rptALL.Name = "DataSet1"

        Dim QueryRet = New DS_K2._v2_Rpt_FromOP_ClassDataTable()
        Dim RowList = New List(Of DS_K2._v2_Rpt_FromOP_ClassRow)()

        RowList = dta.GetData(RptDateUnit.ToString(), dtFilter, Nothing, False, Nothing).ToList()
        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        Else
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
            'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
            If FrmPtr.cbxEnableDateRange.Checked Then
                RowList = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2, True, AggLevel.Day.ToString()).ToList
            Else
                RowList = dta.GetData(RptDateUnit.ToString(), dtFilter, Nothing, False, Nothing).ToList
            End If
            If FrmPtr.SkipK1MIDEnable Then

                If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                    RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
                Else
                    RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
                End If

            Else
                If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                    RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
                End If
            End If
            params.Add(New ReportParameter("YieldUnit", YieldUnit.Ton.ToString))
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        End If

        rptALL.Value = RowList
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)
        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub

    Shared Sub ShowYieldReportK1FromOP_HourOldVersion(dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters.GetYieldByDate_FromOP_HourTableAdapter()

        Dim rptALL = New ReportDataSource

        Dim params() As ReportParameter = Nothing
        Dim tmpObj = New DS_1ke.GetYieldByDate_FromOP_HourDataTable()
        rptALL.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        If DateUnit = RptDateUnit.Day Then


            If FrmPtr.cbxEnableDateRange.Checked Then
                tmpObj = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
            Else
                tmpObj = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
            End If

            'Dim xx = (From item In tmpObj Where item.MachineID > 31 Select item)

            rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).ToList()
            params = {
                     New ReportParameter("DeptStr", DeptStr),
                     New ReportParameter("RptCycle", DateUnit.ToString()),
                     New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月dd日")),
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                     }
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        ElseIf DateUnit = RptDateUnit.Month Then

            If FrmPtr.cbxEnableDateRange.Checked Then
                tmpObj = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
            Else
                tmpObj = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
            End If

            rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).ToList()
            params = {
                     New ReportParameter("DeptStr", DeptStr),
                     New ReportParameter("RptCycle", DateUnit.ToString()),
                     New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月")),
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                     }
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        ElseIf DateUnit = RptDateUnit.Year Then
            If FrmPtr.cbxEnableDateRange.Checked Then
                tmpObj = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
            Else
                tmpObj = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
            End If
            rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()
            params = {
                     New ReportParameter("DeptStr", DeptStr),
                     New ReportParameter("RptCycle", DateUnit.ToString()),
                     New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年")),
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                     }
        End If

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
        End If


        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="dtFilter"></param>
    ''' <param name="DateUnit"></param>
    ''' <param name="_ReportEmbeddedResource"></param>
    ''' <param name="yieldUnit"></param>
    Shared Sub ShowYieldReportK2FromOP_HourOldVersion(dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters.GetYieldByDate_FromOP_HourTableAdapter()

        Dim rptALL = New ReportDataSource

        Dim params() As ReportParameter = Nothing
        Dim tmpObj = New DS_1ke.GetYieldByDate_FromOP_HourDataTable()
        rptALL.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        If DateUnit = RptDateUnit.Day Then


            If FrmPtr.cbxEnableDateRange.Checked Then
                tmpObj = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
            Else
                tmpObj = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
            End If

            'Dim xx = (From item In tmpObj Where item.MachineID > 31 Select item)

            rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).ToList()
            params = {
                     New ReportParameter("DeptStr", DeptStr),
                     New ReportParameter("RptCycle", DateUnit.ToString()),
                     New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月dd日")),
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                     }
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        ElseIf DateUnit = RptDateUnit.Month Then

            If FrmPtr.cbxEnableDateRange.Checked Then
                tmpObj = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
            Else
                tmpObj = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
            End If

            rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).ToList()
            params = {
                     New ReportParameter("DeptStr", DeptStr),
                     New ReportParameter("RptCycle", DateUnit.ToString()),
                     New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年MM月")),
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                     }
            'New ReportParameter("RptParamDate", dtFilter.ToLongDateString()),
            'New ReportParameter("RptParamYieldGoals", {1000, 1500, 3000}),
        ElseIf DateUnit = RptDateUnit.Year Then
            If FrmPtr.cbxEnableDateRange.Checked Then
                tmpObj = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
            Else
                tmpObj = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
            End If
            rptALL.Value = (From item In tmpObj Where (FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" And item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) _
                                                                       Or FrmPtr.cbbxBrand.SelectedItem.ToString() = "*" Select item).AsDataView()
            params = {
                     New ReportParameter("DeptStr", DeptStr),
                     New ReportParameter("RptCycle", DateUnit.ToString()),
                     New ReportParameter("RptDate", FrmPtr.dtPicker1.Value.ToString("yyyy年")),
                     New ReportParameter("AllYieldSum"),
                     New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram"))))
                     }
        End If

        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
        End If


        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub


    Shared Sub Test_ShowYieldReportK1FromOp_ClassByV3Proc(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v3_Rpt_GetDayYieldFromOP_Class_FixVersionTableAdapter()

        Dim rptALL As New ReportDataSource

        rptALL.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        Dim QueryRet = New DS_1ke._v3_Rpt_GetDayYieldFromOP_Class_FixVersionDataTable()
        Dim RowList = New List(Of DS_1ke._v3_Rpt_GetDayYieldFromOP_Class_FixVersionRow)()

        '测试模式 就为True ,因为 旦尼没读上来,为0 ,,产量x0=0
        QueryRet = dta.GetData(dtFilter, True)

        RowList = QueryRet.ToList()

        If FrmPtr.SkipK1MIDEnable Then

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else
                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptALL.Value = RowList

        params.Add(New ReportParameter("AllYieldSum"))
        params.Add(New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram", yieldUnit = YieldUnit.Ton, "ton")))))

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        Else
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
            Return
        End If

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub




    Shared Sub ShowYieldReportK1FromOp_Class(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v1_Rpt_FromOP_ClassTableAdapter()

        Dim rptALL As New ReportDataSource
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptALL.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        Dim QueryRet = New DS_1ke._v1_Rpt_FromOP_ClassDataTable
        Dim RowList = New List(Of DS_1ke._v1_Rpt_FromOP_ClassRow)()

        '日期区间实现区
        If FrmPtr.cbxEnableDateRange.Checked Then
            QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
        Else
            QueryRet = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
        End If

        RowList = QueryRet.ToList()

        If FrmPtr.SkipK1MIDEnable Then

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else
                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptALL.Value = RowList

        params.Add(New ReportParameter("AllYieldSum"))
        params.Add(New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram", yieldUnit = YieldUnit.Ton, "ton")))))

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
            Return
        End If

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub


    Shared Sub ShowYieldReportK2FromOp_Class(params As List(Of ReportParameter), dtFilter As Date, DeptStr As String, DateUnit As RptDateUnit, _ReportEmbeddedResource As String, Optional yieldUnit As YieldUnit = YieldUnit.Gram)
        Dim dt2 = FrmPtr.dtPicker2.Value
        Dim dta = New DS_1keTableAdapters._v1_Rpt_FromOP_ClassTableAdapter()

        Dim rptALL As New ReportDataSource
        FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = _ReportEmbeddedResource
        rptALL.Name = "DataSet1"
        'GetData 函数 1,表示早班,2 表示晚班，（2班制的情况下，如果3班制的话2表示中班）
        Dim QueryRet = New DS_1ke._v1_Rpt_FromOP_ClassDataTable
        Dim RowList = New List(Of DS_1ke._v1_Rpt_FromOP_ClassRow)()

        '日期区间实现区
        If FrmPtr.cbxEnableDateRange.Checked Then
            QueryRet = dta.GetData(RptDateUnit.DateRange.ToString(), dtFilter, dt2)
        Else
            QueryRet = dta.GetData(DateUnit.ToString(), dtFilter, Nothing)
        End If

        RowList = QueryRet.ToList()

        If FrmPtr.SkipK1MIDEnable Then

            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            Else
                RowList = (From item In RowList Where (item.MachineID < 6 Or item.MachineID > 10) Select item).ToList()
            End If

        Else
            If FrmPtr.cbbxBrand.SelectedItem.ToString() <> "*" Then
                RowList = (From item In RowList Where (item.Brand.ToUpper() = FrmPtr.cbbxBrand.SelectedItem.ToString().ToUpper()) Select item).ToList()
            End If
        End If

        rptALL.Value = RowList

        params.Add(New ReportParameter("AllYieldSum"))
        params.Add(New ReportParameter("YieldUnit", CStr((Microsoft.VisualBasic.Switch(yieldUnit = YieldUnit.Metre, "metre", yieldUnit = YieldUnit.Gram, "gram", yieldUnit = YieldUnit.Ton, "ton")))))

        If RowList.Count = 0 Then
            FrmPtr.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSystem.数据为空.rdlc"
        End If
        FrmPtr.ReportViewer1.LocalReport.DataSources.Clear()
        FrmPtr.ReportViewer1.LocalReport.DataSources.Add(rptALL)

        If params IsNot Nothing Then
            FrmPtr.ReportViewer1.LocalReport.SetParameters(params)
        Else
            MessageBox.Show("异常:" + _ReportEmbeddedResource + " 报表参数为空。")
            Return
        End If

        FrmPtr.ReportViewer1.LocalReport.Refresh()
        FrmPtr.ReportViewer1.RefreshReport()
    End Sub
End Class
