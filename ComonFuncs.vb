﻿Imports System.Threading

Public Class ComonFuncs
    Public Shared Sub DelaySync(ms As Int32)
        Dim sw As Stopwatch = New Stopwatch()
        sw.Start()
        While True
            Application.DoEvents()
            If sw.ElapsedMilliseconds > ms Then
                Exit While
            End If
            Thread.Sleep(20)
        End While
    End Sub
End Class
