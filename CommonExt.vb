﻿Imports System.Runtime.CompilerServices

Module CommonExt
    <Extension()>
    Function TianheExtInclude(ByVal obj As Object, ParamArray arrFind() As Object) As Boolean
        Dim arr
        For Each arr In arrFind
            If obj = arr Then
                Return True
            End If
        Next
        Return False
    End Function

    <Extension()>
    Sub ExampleMethod(ByVal ec As Object,
                  ByVal n As Long)
        Console.WriteLine("Extension method")
    End Sub

    <Extension()>
    Public Function TianheExtToDataTable(rows As DataRow()) As DataTable

        If rows Is Nothing Then
            Return New DataTable()
        ElseIf rows.Length = 0 Then
            Return Nothing
        End If

        Dim tmp = rows(0).Table.Clone() '复制DataRow的表结构

        For Each row As DataRow In rows
            tmp.ImportRow(row)  ' 将DataRow添加到DataTable中
        Next

        Return tmp
    End Function
End Module

Public Class Common

    Public Shared Function ToDataTable(rows As DataRow()) As DataTable

        If rows Is Nothing OrElse rows.Length = 0 Then
            Return New DataTable()
        End If
        Dim tmp = rows(0).Table.Clone() '复制DataRow的表结构

        For Each row As DataRow In rows
            tmp.ImportRow(row)  ' 将DataRow添加到DataTable中
        Next

        Return tmp

    End Function
End Class
